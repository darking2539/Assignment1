EESchema Schematic File Version 2
LIBS:NS-HP-GL-rescue
LIBS:power
LIBS:device
LIBS:audio
LIBS:interface
LIBS:NSHPGL
LIBS:NS-HP-GL-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x06 J1
U 1 1 5B06C1A2
P 1650 3500
F 0 "J1" H 1650 3800 50  0000 C CNN
F 1 "Conn_01x06" H 1650 3100 50  0000 C CNN
F 2 "Connectors_Molex:Molex_PicoBlade_53261-0671_06x1.25mm_Angled" H 1650 3500 50  0001 C CNN
F 3 "" H 1650 3500 50  0001 C CNN
F 4 "53261-0671" H 1650 3500 60  0001 C CNN "partno"
	1    1650 3500
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR01
U 1 1 5B0779F1
P 3750 2000
F 0 "#PWR01" H 3750 1850 50  0001 C CNN
F 1 "+3.3V" H 3750 2140 50  0000 C CNN
F 2 "" H 3750 2000 50  0001 C CNN
F 3 "" H 3750 2000 50  0001 C CNN
	1    3750 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B077B2B
P 2050 3800
F 0 "#PWR02" H 2050 3550 50  0001 C CNN
F 1 "GND" H 2050 3650 50  0000 C CNN
F 2 "" H 2050 3800 50  0001 C CNN
F 3 "" H 2050 3800 50  0001 C CNN
	1    2050 3800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5B07821F
P 7200 4850
F 0 "#PWR03" H 7200 4600 50  0001 C CNN
F 1 "GND" H 7200 4700 50  0000 C CNN
F 2 "" H 7200 4850 50  0001 C CNN
F 3 "" H 7200 4850 50  0001 C CNN
	1    7200 4850
	1    0    0    -1  
$EndComp
$Comp
L BNC P1
U 1 1 5B078A1A
P 3100 4450
F 0 "P1" H 3110 4570 60  0000 C CNN
F 1 "U.FL" V 3210 4390 40  0000 C CNN
F 2 "HG_Devices:U.FL" H 3100 4450 60  0001 C CNN
F 3 "" H 3100 4450 60  0000 C CNN
F 4 "U.FL-R-SMT(10)" H 3100 4450 60  0001 C CNN "partno"
	1    3100 4450
	-1   0    0    -1  
$EndComp
$Comp
L BNC P2
U 1 1 5B078B69
P 3850 4450
F 0 "P2" H 3860 4570 60  0000 C CNN
F 1 "RPSMA_Female" V 3960 4390 40  0000 C CNN
F 2 "HG_Devices:RPSMA_THT_Female_RA" H 3850 4450 60  0001 C CNN
F 3 "" H 3850 4450 60  0000 C CNN
F 4 "CONREVSMA002-G" H 3850 4450 60  0001 C CNN "partno"
	1    3850 4450
	1    0    0    -1  
$EndComp
Text Notes 3300 4200 0    60   Italic 0
RF trace
Text Notes 1300 3150 0    60   ~ 0
Pixhawk_Telem1
$Comp
L NSHPGL U11
U 1 1 5B07C76F
P 5850 3850
F 0 "U11" H 6350 5000 60  0000 C CNN
F 1 "NSHPGL" H 5850 3700 60  0000 C CNN
F 2 "HG_Devices:NS-HP-GL" H 6350 5000 60  0001 C CNN
F 3 "" H 6350 5000 60  0001 C CNN
F 4 "NS-HP-GL" H 5850 3850 60  0001 C CNN "partno"
	1    5850 3850
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR04
U 1 1 5B07800A
P 4600 3350
F 0 "#PWR04" H 4600 3200 50  0001 C CNN
F 1 "+3.3V" H 4600 3490 50  0000 C CNN
F 2 "" H 4600 3350 50  0001 C CNN
F 3 "" H 4600 3350 50  0001 C CNN
	1    4600 3350
	1    0    0    -1  
$EndComp
$Comp
L LD1117S33TR_SOT223 U1
U 1 1 5B0B84A6
P 3100 2000
F 0 "U1" H 2950 2125 50  0000 C CNN
F 1 "LD1117S33TR_SOT223" H 2700 2250 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-223-3_TabPin2" H 3100 2200 50  0001 C CNN
F 3 "" H 3200 1750 50  0001 C CNN
F 4 "LD1117S33TR" H 3100 2000 60  0001 C CNN "partno"
	1    3100 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5B0B874C
P 3100 2300
F 0 "#PWR05" H 3100 2050 50  0001 C CNN
F 1 "GND" H 3100 2150 50  0000 C CNN
F 2 "" H 3100 2300 50  0001 C CNN
F 3 "" H 3100 2300 50  0001 C CNN
	1    3100 2300
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5B0BAB5F
P 2550 2150
F 0 "C1" H 2400 2350 50  0000 L CNN
F 1 "100nF" H 2575 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2588 2000 50  0001 C CNN
F 3 "" H 2550 2150 50  0001 C CNN
F 4 "MC0603F104Z250CT" H 2550 2150 60  0001 C CNN "partno"
	1    2550 2150
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5B0BAE12
P 3550 2150
F 0 "C2" H 3400 2350 50  0000 L CNN
F 1 "10uF" H 3575 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3588 2000 50  0001 C CNN
F 3 "" H 3550 2150 50  0001 C CNN
F 4 "GRM21BR61H106KE43L" H 3550 2150 60  0001 C CNN "partno"
	1    3550 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5B0BAE54
P 3550 2300
F 0 "#PWR06" H 3550 2050 50  0001 C CNN
F 1 "GND" H 3550 2150 50  0000 C CNN
F 2 "" H 3550 2300 50  0001 C CNN
F 3 "" H 3550 2300 50  0001 C CNN
	1    3550 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5B0BAEBE
P 2550 2300
F 0 "#PWR07" H 2550 2050 50  0001 C CNN
F 1 "GND" H 2550 2150 50  0000 C CNN
F 2 "" H 2550 2300 50  0001 C CNN
F 3 "" H 2550 2300 50  0001 C CNN
	1    2550 2300
	1    0    0    -1  
$EndComp
Text Label 6850 4550 0    60   ~ 0
RX_NSHPGL
Text Label 6850 4400 0    60   ~ 0
TX_NSHPGL
$Comp
L +3.3V #PWR08
U 1 1 5B0E20BF
P 6850 5000
F 0 "#PWR08" H 6850 4850 50  0001 C CNN
F 1 "+3.3V" H 6850 5140 50  0000 C CNN
F 2 "" H 6850 5000 50  0001 C CNN
F 3 "" H 6850 5000 50  0001 C CNN
	1    6850 5000
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR09
U 1 1 5B151086
P 1350 2400
F 0 "#PWR09" H 1350 2150 50  0001 C CNN
F 1 "GND" H 1350 2250 50  0000 C CNN
F 2 "" H 1350 2400 50  0001 C CNN
F 3 "" H 1350 2400 50  0001 C CNN
	1    1350 2400
	1    0    0    -1  
$EndComp
Text Label 1950 3300 0    60   ~ 0
5V
Text Label 1350 2000 0    60   ~ 0
5V
Text Notes 2350 3400 0    60   ~ 0
Pixhawk_TX
Text Notes 2350 3500 0    60   ~ 0
Pixhawk_RX
$Comp
L Conn_01x06 J2
U 1 1 5B16141F
P 9250 4100
F 0 "J2" H 9250 4400 50  0000 C CNN
F 1 "Conn_01x06" H 9250 3700 50  0000 C CNN
F 2 "Connectors_Molex:Molex_PicoBlade_53261-0671_06x1.25mm_Angled" H 9250 4100 50  0001 C CNN
F 3 "" H 9250 4100 50  0001 C CNN
	1    9250 4100
	1    0    0    -1  
$EndComp
Text Notes 8800 3700 0    60   ~ 0
Pixhawk_Telem2
Text Notes 8650 4800 0    60   Italic 12
Rover+Base Connection
Text Notes 8350 4100 0    60   ~ 0
Rover
Text Notes 8350 4400 0    60   ~ 0
Base
Text Label 8950 3900 0    60   ~ 0
5V
$Comp
L MINI-USB-SHIELD-32005-201 X1
U 1 1 5B1627C4
P 1150 2200
F 0 "X1" H 1450 1850 50  0000 L BNN
F 1 "MINI-USB-SHIELD-32005-201" H 550 2600 50  0000 L BNN
F 2 "Connectors_USB:USB_Micro-B_Molex_47346-0001" H 1150 2350 50  0001 C CNN
F 3 "" H 1150 2200 60  0000 C CNN
F 4 "47346-0001" H 1150 2200 60  0001 C CNN "partno"
	1    1150 2200
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5B16748F
P 9050 4400
F 0 "#PWR010" H 9050 4150 50  0001 C CNN
F 1 "GND" H 9050 4250 50  0000 C CNN
F 2 "" H 9050 4400 50  0001 C CNN
F 3 "" H 9050 4400 50  0001 C CNN
	1    9050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3400 4300 1950
Wire Wire Line
	4300 1950 7400 1950
Wire Wire Line
	4450 2100 4450 3500
Wire Wire Line
	4450 2100 7200 2100
Wire Wire Line
	2050 3800 1850 3800
Wire Wire Line
	6850 4850 7200 4850
Connection ~ 7000 4850
Wire Wire Line
	3250 4450 3700 4450
Wire Wire Line
	7400 1950 7400 4550
Wire Wire Line
	4800 3350 4600 3350
Wire Wire Line
	4800 3350 4800 3500
Wire Wire Line
	7000 4850 7000 3500
Wire Wire Line
	7000 3500 6850 3500
Wire Wire Line
	3400 2000 3750 2000
Connection ~ 3550 2000
Wire Wire Line
	2200 2000 2800 2000
Connection ~ 2550 2000
Wire Wire Line
	4300 3400 1850 3400
Wire Wire Line
	4450 3500 1850 3500
Wire Wire Line
	1350 2000 2550 2000
Wire Wire Line
	7400 4550 6850 4550
Wire Wire Line
	6850 4100 9050 4100
Wire Notes Line
	8750 3750 8750 4650
Wire Notes Line
	8750 4650 9650 4650
Wire Notes Line
	9650 4650 9650 3750
Wire Notes Line
	9650 3750 8750 3750
Connection ~ 7200 4400
Wire Wire Line
	7200 2100 7200 4400
Wire Wire Line
	8850 4400 6850 4400
Wire Wire Line
	8850 4000 8850 4400
Connection ~ 2200 2000
Wire Wire Line
	2200 1450 2200 3300
Wire Wire Line
	2200 3300 1850 3300
Wire Wire Line
	2200 1450 8800 1450
Wire Notes Line
	2900 4100 2900 4900
Wire Notes Line
	2900 4900 4100 4900
Wire Notes Line
	4100 4900 4100 4100
Wire Notes Line
	4100 4100 2900 4100
Wire Wire Line
	8850 4000 9050 4000
Wire Wire Line
	8800 3900 9050 3900
Wire Wire Line
	8800 1450 8800 3900
Wire Wire Line
	4800 2900 4700 2900
Wire Wire Line
	4750 2900 4750 3200
Wire Wire Line
	4750 3200 4800 3200
Connection ~ 4750 2900
$Comp
L GND_a #PWR011
U 1 1 5B1F90AD
P 3100 4650
F 0 "#PWR011" H 3100 4600 40  0001 C CNN
F 1 "GND_a" H 3100 4500 60  0000 C CNN
F 2 "" H 3100 4600 60  0000 C CNN
F 3 "" H 3100 4600 60  0000 C CNN
	1    3100 4650
	1    0    0    -1  
$EndComp
$Comp
L GND_a #PWR012
U 1 1 5B1F947E
P 3850 4650
F 0 "#PWR012" H 3850 4600 40  0001 C CNN
F 1 "GND_a" H 3850 4500 60  0000 C CNN
F 2 "" H 3850 4600 60  0000 C CNN
F 3 "" H 3850 4600 60  0000 C CNN
	1    3850 4650
	1    0    0    -1  
$EndComp
$Comp
L GND_a #PWR013
U 1 1 5B1F954C
P 4700 2900
F 0 "#PWR013" H 4700 2850 40  0001 C CNN
F 1 "GND_a" H 4700 2750 60  0000 C CNN
F 2 "" H 4700 2850 60  0000 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 2900
	0    1    1    0   
$EndComp
$Comp
L GND_a #PWR014
U 1 1 5B1F9C38
P 3750 5500
F 0 "#PWR014" H 3750 5450 40  0001 C CNN
F 1 "GND_a" H 3750 5350 60  0000 C CNN
F 2 "" H 3750 5450 60  0000 C CNN
F 3 "" H 3750 5450 60  0000 C CNN
	1    3750 5500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5B1F9CB3
P 2950 5500
F 0 "#PWR015" H 2950 5250 50  0001 C CNN
F 1 "GND" H 2950 5350 50  0000 C CNN
F 2 "" H 2950 5500 50  0001 C CNN
F 3 "" H 2950 5500 50  0001 C CNN
	1    2950 5500
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B1F9CC4
P 3350 5500
F 0 "R1" V 3430 5500 50  0000 C CNN
F 1 "0k" V 3350 5500 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805" V 3280 5500 50  0001 C CNN
F 3 "https://www.digikey.com/product-detail/en/vishay-dale/CRCW06030000Z0EB/541-2968-1-ND/6073611" H 3350 5500 50  0001 C CNN
F 4 "CRCW06030000Z0EB " H 3350 5500 60  0001 C CNN "partno"
	1    3350 5500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 5500 3200 5500
Wire Wire Line
	3500 5500 3750 5500
Wire Notes Line
	2750 5350 2750 5750
Wire Notes Line
	2750 5750 3950 5750
Wire Notes Line
	3950 5750 3950 5350
Wire Notes Line
	3950 5350 2750 5350
Text Notes 2900 5300 0    60   ~ 0
Star ground\n
$EndSCHEMATC
